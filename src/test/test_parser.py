import unittest
from pathlib import Path
from datetime import datetime
from ..main.common.parser import *


class ParserTestCase(unittest.TestCase):
    def setUp(self):
        self.filename = "test.json"
        self.root_dir = str(Path().absolute())

    def test_parser(self):
        file = self.root_dir + "/" + self.filename
        items = read_from_file(file)

        i = Item(0, 'test item', datetime.now().timestamp())

        if items.count(i) == 0:
            items.append(Item(0, 'test item', datetime.now().timestamp()))

        write_to_file(file, items)


if __name__ == '__main__':
    unittest.main()
