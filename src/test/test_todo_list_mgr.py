from unittest import TestCase
from ..main.data.item import Item
from src.main.applications.todo_list_mgr import TodoListMgr
from datetime import datetime


class TestTodoListMgr(TestCase):

    def setUp(self):
        self.manager = TodoListMgr("test.json")

    def test_read(self):
        self.manager.read()
        self.assertTrue(len(list(self.manager.filter_items(lambda i: True))) >= 0)

    def test_write(self):
        self.manager.read()
        self.manager.remove_all_items()
        self.manager.write()
        self.assertTrue(len(list(self.manager.filter_items(lambda i: True))) == 0)
        title = "item 1"
        self.manager.add_item(title)
        self.manager.write()
        self.manager.read()
        self.assertTrue(len(list(self.manager.filter_items(lambda i: i.get_title() == title))) == 1)
        self.manager.remove_all_items()
        self.manager.write()

    def test_add_item(self):
        self.manager.read()
        title = "item 2"
        self.manager.add_item(title)
        length = len(list(self.manager.filter_items(lambda i: i.get_title() == title)))
        self.assertTrue(length > 0)
        self.manager.add_item(title)
        self.assertTrue(len(list(self.manager.filter_items(lambda i: i.get_title() == title))) == length)

    def test_edit_item_title(self):
        self.manager.read()
        title = "item 2"
        self.manager.add_item(title)

        new_item = Item(0, "item 2 modified", datetime.now().timestamp())
        self.manager.edit_item_title(lambda i: i.get_id() == 0, title)
        length = len(list(self.manager.filter_items(lambda i: i == new_item)))
        self.assertTrue(length > 0)

    def test_remove_item(self):
        self.manager.read()
        title = "item 3"
        self.manager.add_item(title)

        self.manager.remove_item(lambda i: i.get_title() == title)
        length = len(list(self.manager.filter_items(lambda i: i.get_title() == title)))
        self.assertTrue(length == 0)

    def test_toggle(self):
        self.manager.read()
        title = "item 4"
        self.manager.add_item(title)
        for it in self.manager.filter_items(lambda i: i.get_title() == title):
            self.assertFalse(it.get_done())

        self.manager.toggle(lambda i: i.get_title() == title)

        for it in self.manager.filter_items(lambda i: i.get_title() == title):
            self.assertTrue(it.get_done())

        for it in self.manager.filter_items(lambda i: i.get_title() == title):
            self.assertFalse(not it.get_done())

