import unittest
import datetime
from src.main.data.item import Item


class BaseTestCase(unittest.TestCase):
    def test_item_creation(self):
        print('\n')
        item = Item(0, "test", datetime.datetime.now().timestamp())
        self.assertEqual(item.get_id(), 0)
        self.assertEqual(item.get_done(), False)
        self.assertEqual(item.get_title(), 'test')

        items = [item]

        f_items = filter(lambda i: i.get_title() == 'test_', items)

        for f_item in f_items:
            print(f_item)


if __name__ == '__main__':
    unittest.main()
