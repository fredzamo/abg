class Item:
    def __init__(self, id, title, timestamp):
        self.__id = id
        self.__title = title
        self.__timestamp = timestamp
        self.__done = False

    def get_id(self):
        return self.__id

    def get_title(self):
        return self.__title

    def get_timestamp(self):
        return self.__timestamp

    def get_done(self):
        return self.__done

    def set_title(self, title):
        self.__title = title

    def set_timestamp(self, timestamp):
        self.__timestamp = timestamp

    def toggle(self, toggle):
        self.__done = toggle

    def to_dict(self):
        return {"id": self.__id, "title": self.__title, "done": self.__done, "timestamp": self.__timestamp}

    def __str__(self):
        return 'Item(id=' + str(self.__id) \
               + ', title=' + self.__title \
               + ', done=' + str(self.__done) \
               + ', timestamp=' + str(self.__timestamp) \
               + ')'

    def __eq__(self, other):
        if isinstance(other, Item):
            return self.__title.lower() == other.get_title().lower()
        return False

    def __ne__(self, other):
        return not self.__eq__(other)
