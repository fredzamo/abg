import argparse
import os
from .applications.todo_list_mgr import TodoListMgr


class Main:
    def __init__(self):
        pass

    def start(self):
        usage_msg = f'python3 -m {str(__file__).replace(".py", "").replace(f"{os.getcwd()}/", "").replace("/", ".")} [options]'
        parser = argparse.ArgumentParser(add_help=False, usage=usage_msg)
        parser.add_argument('-h', action='help', default=argparse.SUPPRESS,
                            help='show all possible actions')
        parser.add_argument("-l", action="store_true",
                            help="show all TODOs sorted by descending timestamp")
        parser.add_argument("-a", action='store', help="add a TODO", type=str, nargs=1)
        parser.add_argument("-e", action='store', help="edit a TODO", type=str, nargs=2)
        parser.add_argument("-d", action='store', help="delete a TODO", type=int, nargs=1)
        parser.add_argument("-t", action='store', help="toggle a TODO", type=int, nargs=1)
        parser.add_argument("-s", action='store', help="find string in TODO title", type=str, nargs=1)

        args = parser.parse_args()

        options = {'show': args.l, 'add': args.a, 'edit': args.e, 'delete': args.d, 'toggle': args.t, 'search': args.s}

        filter_options = dict((k, v) for k, v in options.items() if v is not None)

        if len(filter_options) == 1 and not filter_options['show']:
            print("ERROR: no options provided")
            parser.print_help()
            exit(1)

        if len(filter_options) > 1 and filter_options['show']:
            print("ERROR: to many options provided")
            parser.print_help()
            exit(1)

        manager = TodoListMgr('test.json')
        manager.read()

        for key, value in filter_options.items():
            if key == 'show' and value:
                manager.show_items()
            if key == 'add':
                manager.add_item(str(value[0]))
                manager.write()
            if key == 'edit':
                try:
                    id = int(value[0])
                    title = value[1]
                    manager.edit_item_title(lambda i: i.get_id() == id, title)
                    manager.write()
                except ValueError:
                    print("ERROR: first argument MUST be an int")
                    parser.print_help()
                    exit(1)
            if key == 'delete':
                manager.remove_item(lambda i: i.get_id() == value[0])
                manager.write()
            if key == 'toggle':
                manager.toggle(lambda i: i.get_id() == value[0])
                manager.write()
            if key == 'search':
                for item in manager.filter_items(lambda i: value[0] in i.get_title()):
                    print(item)


if __name__ == '__main__':
    m = Main()
    m.start()
