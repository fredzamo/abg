from ..data.item import Item


def validate(item: Item):
    valid = __validate_item_content(item) and __validate_title_length(item)
    return valid


# check item content is not None
def __validate_item_content(item: Item):
    return item is not None \
           and item.get_title() is not None \
           and item.get_timestamp() is not None


# check item length >= 5 characters
def __validate_title_length(item: Item):
    ret = len(item.get_title()) >= 5
    if not ret:
        print("ERROR: the item title must be >= 5 characters")
    return ret
