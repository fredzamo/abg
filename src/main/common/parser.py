import json
from ..data.item import Item
from .item_validator import validate


def read_from_file(filename):
    items = []
    try:
        with open(filename, 'r') as json_file:
            data = json.load(json_file)
            for d in data:
                item = Item(d['id'], d['title'], d['timestamp'])
                item.toggle(d['done'])
                if validate(item):
                    items.append(item)
                else:
                    print("item not valid")
    except FileNotFoundError:
        pass
    except OSError:
        print('error occurred')
    finally:
        return items


def write_to_file(filename, items):
    with open(filename, 'w') as outfile:
        results = [item.to_dict() for item in items]
        json.dump(results, outfile, indent=2)
