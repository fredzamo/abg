from ..common.parser import *
from ..common.item_validator import validate
from datetime import datetime


class TodoListMgr:
    def __init__(self, filename):
        # 'path' is used to point to same json file from every directory the program is called
        path = __file__[:str(__file__).rfind("src")]
        self.__filename = path + filename
        self.__items = []

    # method used to collect items from file
    def read(self):
        self.__items = read_from_file(self.__filename)

    # method used to write to file
    def write(self):
        write_to_file(self.__filename, self.__items)

    # add Item after validation
    def add_item(self, title):
        item = Item(self.__find_first_valid_id(), title, datetime.now().timestamp())
        if self.__items.count(item) == 0:
            if validate(item):
                self.__items.append(item)
        else:
            print("ERROR: item title already inserted")

    # edit items based provided predicate with title and timestamp taken from provided Item if valid
    def edit_item_title(self, predicate, title):
        item = Item(0, title, datetime.now().timestamp())
        if self.__items.count(item) == 0:
            if validate(item):
                for i in self.filter_items(predicate):
                    i.set_title(item.get_title())
                    i.set_timestamp(item.get_timestamp())
        else:
            print("ERROR: item title already inserted")

    # remove an Item based predicate
    def remove_item(self, predicate):
        for i in self.filter_items(predicate):
            self.__items.remove(i)

    def remove_all_items(self):
        self.__items.clear()

    # toggle items found with provided predicate
    def toggle(self, predicate):
        for i in self.filter_items(predicate):
            i.toggle(not i.get_done())

    def filter_items(self, predicate):
        return filter(predicate, self.__items)

    def show_items(self):
        for item in sorted(self.__items, key=lambda i: i.get_timestamp(), reverse=True):
            print(item)

    def __find_first_valid_id(self):
        id = 0;
        for count, v in enumerate(sorted(self.__items, key=lambda i: i.get_id(), reverse=False)):
            if count != v.get_id():
                id = count
                break
            else:
                id = id + 1;
        return id
